# TODO

## Grafika
- [ ] 30 robot spriteů + střely + 1 hráč
- [ ] 10 floor tiles
- [ ] 5 floor texture
- [ ] 5 obstackles
- [x] walls
- [ ] open space abyss
- [x] logo circuit board like outlines
- [ ] map annotation description
- [ ] player status bar (shop, inventory, equipment, stats, artifacts...)

## Kód

## Optimalizace
- [ ] ne-clearovat scénu dláždic, když se startX a startY nezmění (jen normálně updatovat souřadnice)
- [ ] registrovat setLoop v Map()
- [ ] registrovat eventy v Map()
- [ ] vyřešit proměnnou pohybu
- [ ] Map.move() - pohyb hráče vs pohyb mapy vs out-of-map-bounds

## Artefakty
- [ ] rozdělit artefakty do levelů
- [ ] ajatolláh Chomejní
- [ ] obrázkové přílohy
- [ ] reklama a viry

### Hráč
- [x] kamera hráče
- [x] pohyb hráče
- [x] otáčení hráče

### Render mapy

- [ ] floor tiles
- [ ] floor textures
- [ ] obstackles (walls) ?
- [ ] open space abyss
- [ ] nepřátelé (identifikace) ?

### Kolize

- [ ] kolize stěn ?
- [ ] kolize s nepřáteli ?
- [ ] kolize střel ?

https://twitter.com/verge/status/684773028459220992
