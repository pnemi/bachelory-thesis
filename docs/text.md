# Obsah

- Úvod
  - zásady
- O hře
  - téma
  - příběh
- Platforma
  - desktop
  - ovládání
- Vizuální podoba
  - dláždice
  - Tiled
- Struktura
  - herní databáze
- Canvas / oCanvas

## Úvod

Vždy jsem chtěl vytvořit hru. Bakalářská práce pro mě byla příležitostí, jak se o to pokusit. Zásady pro její vznik se nakonec ustálily na desktopové vesmírné akční arkádové 2D hře s RPG prvky. Hráč bude prostřednictvím svého robota ničit nepřátelské roboty a plnit mise. Za herní měnu bude mít poté možnost vylepšit si vlastnosti jeho robota pomocí předmětů z herního obchodu.

## O hře

The Journer of Karel neboli Karlova Cesta, pojmenovaná podle spisovatele Karla Čapka, který poprvé použil a popularizoval slovo robot ve své divadelní hře R.U.R., je zasazena do postapokalyptické doby po zániku lidstva, zničení naší planety, dosažení technologické singularity a tématicky se dotéká aktuálně stále více diskutovaného fenoménu umělé inteligence, kterým jsem osobně velmi fascinován.

Hlavním hrdinou příběhu je vesmírná sonda Karel, která byla vyslána ze své domovské planety již autonomních robotů na planetu Zemi původně za účelem těžby, co však Karel nalézá nejsou ložiska vzácného prvku užívaného při výrobě samoopravovacího materiálu pro nové série robotů, ale artefakty, pozůstatky našeho světa v podobě autentických záznamů z reálných zdrojů, jakými jsou novinové články, knihy, hudba, dokumenty a další zmínky o událostech, které svým způsobem změnily svět navždy. Hra se tímto stává restrospekcí naší existence, poskytuje hráči nestereotypní gameplay a je přinejmenším nesnadné zařadit ji do nějakého žánru.


Artefakty pojednávají zejména o temných momentech lidstva (např.: počátek 2. světové války, vývoj první atomové bomby, teroristické útoky z 11. září a na redakci satiristického časopisu Charlie Hebdo), rasových poměrech v naší společnosti (např.: zvolení prvního Afroamerického prezidenta Spojených států Baracka Obamy a proslov Martina Luthera Kinga), vývoji v informatice (např.: von Neumannova koncepce počítače, třídící algoritmus Quicksort Tonyho Hoare, představení operačního systému MS-DOS, Linuxu a počítače Macintosh), vědeckofantastických věcech (např.: divadelní hra Karla Čapka R.U.R. a film Matrix), myšlenek zabývajících se umělou inteligencí (např.: vytvoření disciplíny AI Johnem McCarthym, Turingův test, komentáře Stephena Hawkinga a Elona Muska), umění (např.: poezie Edgara Allana Poea, Shakespearova hra Romeo a Julie, obraz Leonarda da Vinci), hudbě (např.: skupina Queen, Beatles, Justin Bieber a Kurt Kobain) zmínky o politice a státních zřízení (např.: dokument Magna Carta alias základní kámen demokracie, Günther Schabowski a pád Berlínské zdi) a další (např.: první let bratří Wrightů, vypuštění Sputniku I na oběžnou dráhu, závěť Alfreda Nobela a popravení Saddáma Husseina).

V každém z deseti levelů se cílem nyní stává nejen zničit pozůstalé nepřátelské existence, které Karlovi stojí v cestě, ale posbírat hlavně všechny artefakty na mapě. Artefakty jsou seskupeny podle určité souvislosti a každému levelu je přířazen příběh, jehož znění se hráč dozvídá před jeho začátkem pomocí anotace. Každý artefakt má přiřazen jakými emocemi může svým příběhem na hráče působit. Získáváním těchto artefaktů se zvyšuje Karlův emoční level, což zároveň reflektuje emoční dojem samotného hráče. Herní databáze navíc disponuje hláškami, které jsou ohodnoceny kritérii na Karlův emoční level. V zájmu hráče je postupně zvyšovat tyto emoce, na čím vyšší emoční úrovni se Karel nachází, tím říká méně strojové a více lidské či dokonce slangové hlášky.
Hra využívá model kategorizace emocí amerického psychologa Paula Ekmana do 6 základních.

Zabíjením nepřátel získává hráč odměny ve formě zkušeností, za které lze v herním obchodě zakoupit předměty zvyšující základní atributy jako zdraví, sílu a rychlost střelby a obranu.

## Platforma

Hra běží ve webovém prohlížeči a je určena pro desktopová zařízení. K jejímu ovládání je zapotřebí klávesnice pro pohyb robota na mapě a myš pro střelbu.

## Vizuální podoba

Veškerá herní grafická podoba je definována v ortogonálním 2D stylu. Všechny prvky, včetně map, obrázků robotů, předmětů z herního obchodu a artefaktů, jsou složeny z malých čtverečků o velikosti 32 pixelů, kterým se říká dláždice, respektive sprites v případě robotů a zmíněných předmětů.

Ačkoliv se kompozice mapy z čtverců může zdát velmi limitující, opak je pravdou. S trochou šikovnosti lze mapu uvést do podoby, kdy hráč nebude mít nejmenší tušení, že je hra tvořena ze čtverců skládaných pravidelně vedle sebe. Různé kombinace několika pár dláždic můžou ve výsledku znázornit stejný terén z několika různých perspektiv. Skrz tento fakt považuji grafiku za největší výzvu, která mi paradoxně zabrala nejvíc času. Nakonec jsem ovšem musel udělat kompromis a přenést se přes to, s jakým výsledkem to celé dopadlo.

Grafický návrh jsem realizoval ve vektorovém editoru Sketch na OS X. Výsledný produkt jsem mohl následně rasterizovat i do větších rozlišení pro zařízení, které disponují s vysokou hustotou pixelů na palec (dpi). U zařízení Apple je tato vlastnost známa pod komerčním názvem Retina.

### Menu

**IMG - MENU**

Menu je první věc, se kterou se hráč setká. Po jeho načtení je mu nabídnut absolutní přehled na jedné obrazovce.

Vlevo nahoře se nachází rozbalovací nabídka spojená s akcemi, které lze provádět s uživatelským profilem. Aktuálně se zde nachází možnost resetování profilu ve smyslu vynulování nasbíraných zkušeností, zakoupených předmětů a nasbíraných artefaktů.

Vpravo nahoře je hráč seznámen s herním ovládáním.

Uprostřed menu je umístěno jednoduché typografické logo. Slovo Karel je nápadně nastylováno, aby připomínalo elektrický obvod.

O trochu níže lze spatřit vylistovaných 10 základních misí, jejichž splnění je hráčovým cílem. Mise jsou rovněž vybaveny efektní a přehlednou vizualizací, radiálním pruhem, který prezentuje procentuální postup v dané misi, přesněji řečeno, kolik artefaktů hráč na dané mapě už posbíral. Každá mise má svůj název napovídající její charakter, který je naplno odkryt po najetí myší na vybranou misi, v tomto okamžiku se hráči zobrazí celý příběh této mise, kterou může po kliknutí začít hrát.

Na dolním okraji obrazovky je situovaný přehled o postupu hráče ve hře. Vedle tlačítek pro zobrazení artefaktů nebo herního obchodu jsou zobrazené informace o emočním stavu Karla a počet získaných zkušeností. Tento pruh překrývá nejen herní menu, ale i mapu, tím je zajištěn přístup k přehledu nad postupem ve hře odkudkoliv.


### Inventář

Přístup ke všemu, co se herního postupu týče, lze přes herní komponentu, kterou jsem nazval inventář, buď pomocí tlačítek v přehledu herního postupu na spodním okraji obrazovky nebo vyznačenými klávesovými zkratkami.

Inventář je dostupný k zobrazení odkudkoliv, hlavním záměrem bylo totiž mít možnost prohlížet si nasbírané artefakty nebo nakupovat bez nutnosti být případně rozptýlen aktuálně probíhající misí na pozadí.


#### Artefakty

**IMG - ARTIFACTS PREVIEW**

Levou stranu inventáře vyplňuje přehled získaných artefaktů. Jednoduché a kontrastní dvouslopcové rozvržení nejprve dává na výběr detail kterého artefaktu si hráč chce pohlédnout a poté jsou jeho informace zobrazeny vedle tohoto výběru.

Titulek artefaktu doplňuje rok jeho události. Jestliže je u artefaktu i příloha ve formě obrázku, bude zobrazen její náhled. Náhled je možné zvětšit kliknutím. Nedílnou součástí jsou ovšem štítky, které podtrhují, jaký konkrétní emoční zážitek může hráč z této události mít. Štítky emocí, kterých se to týká jsou viditelně více výrazné než ostatní.

Pod štítky následuje krátká autentická zmínka spojená s událostí. Způsob, jakým jsem úryvky z různých zdrojů získával je spíše náhodný. Buď se jedná o událost, která je obecně známá, ale například už ne, jak se stala nebo je to událost, na kterou se už zapomnělo, přesto je však zásadní kvůli tomu, že ovlivnila společnost natolik, že se změnilo mnohé.

Výběr samotných artefaktů je rovněž tématicky přímo spojen s tématem hry.

#### Obchod

**IMG - ITEMS PREVIEW**

Jedním z elementů, které si hra osvojuje je možnost vylepšení robota Karla díky předmětům z herního obchodu, který je k nalezení na pravé straně v inventáři. Všechny dostupné předměty jsou přehledně vyskládány vedle sebe v dláždicovém uspořádání.

**IMG - ITEM DETAIL**

Informace o žádaném předmětu může hráč získat najetím myší, tím se mu zobrazí box s názvem předmětu, typem, cenou, vlastnostmi, které předmět vylepší a relevantním popiskem. Dvojím poklepáním myší si předmět zakoupí, v případě neúspěchu se hráči zobrazí hláška, která upozorní na nedostatek zkušeností nebo na to, že hráč už tento předmět nebo předmět stejného typu zakoupil.

**jaké jsou typy a co zvyšují**


### Tiled



## Canvas

Hra je vytvořena za použití HTML5 Canvas technologie a JavaScriptu. Je možné ji hrát v jakémkoliv webovém prohlížeči podporující Canvas API, které má velkou podporu ve všech hlavních webových prohlížečích už několik let, zejména i v nepopulárním Internet Exploreru a to již od verze 9, který vyšel před pěti lety. V takovém případě mi Canvas nabízí možnost udělat plně multiplatformní hru bez žádných kompromisů.
