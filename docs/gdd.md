# GDD

_Game Design Document (GDD)_

- [Petr Němeček](http://petrnemecek.net/)
- [UPOL ~ Department of Computer Science](http://www.inf.upol.cz)

## Summary
- HTML5 Canvas
- Futuristic space action shooter / Shoot 'em up
- 2D top-down view
- Tile-based


## Game Interface

### Gameplay

- At first player creates / chooses his profile (via entering / selecting his name) which is saved / loaded locally (HTML5 Offline Storage API)
- There are 10 built-in mission locations in the game
- Robot KAREL is the main character of the game
- Robot fights its enemies and finds artifacts
- Artifacts are randomly spread around the map
- Enemies drop all kind of items and equipment
- Level is fully completed when:
    - Player acquires all artifacts
    - Player defeats raid boss
- All missions are unlocked although player faces harder enemies in the next missions

### Combat and Movement

- Robot moves only on floor tiles
- Robot dies when stepping outside tiles area (on empty space)
- Robot fights enemy robots using its weapons
- Robot' shield protection reduces upon collision with another enemy (exploding) robot and when it gets shot by bullets or missiles
- Robot moves freely all across the entire space (level resp.) and meets its enemies, continuously uncovers the map
- Enemies start attacking once they become visible on canvas and behold your character
- Enemies are being respawned after...

### Control Scheme

_**Mouse**_

- <kbd>MOUSE</kbd> — Aiming
- <kbd>Left Button</kbd> - Shooting

_**Keyboard**_

- <kbd>WSAD</kbd> — Moving up, down, left, right
- <kbd>P</kbd> — Pause

## Custom Levels

- Besides finishing basic levels, player has an option to load additional custom mission with desired map, enemies and weapons
- Items and enemies are available only when they are unlocked by finishing certain basic levels
- Maps are loaded via configuration JSON type-of files
